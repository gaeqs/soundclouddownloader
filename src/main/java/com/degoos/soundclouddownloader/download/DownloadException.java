package com.degoos.soundclouddownloader.download;

public class DownloadException extends RuntimeException {

	public DownloadException(String message) {
		super(message);
	}

}
