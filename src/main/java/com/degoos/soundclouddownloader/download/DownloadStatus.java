package com.degoos.soundclouddownloader.download;

public enum DownloadStatus {

	READY, DOWNLOADING, DONE

}
