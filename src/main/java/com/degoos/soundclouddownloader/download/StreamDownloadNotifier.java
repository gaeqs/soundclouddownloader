package com.degoos.soundclouddownloader.download;

public interface StreamDownloadNotifier {

	void onStart(StreamDownload download);

	void onDownload(StreamDownload download);

	void onFinish(StreamDownload download);

}
