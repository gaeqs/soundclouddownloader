package com.degoos.soundclouddownloader.download;

import com.degoos.soundclouddownloader.util.HTMLUtils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;

public class StreamDownload {

	private static int BUF_SIZE = 4 * 1024;

	private URL url;
	private File target;
	private StreamDownloadNotifier notifier;

	private int length, count;
	private DownloadStatus status;

	public StreamDownload(URL url, File target, StreamDownloadNotifier notifier) {
		assert url != null && target != null;
		this.url = url;
		this.target = target;
		this.notifier = notifier;
		this.length = 0;
		this.count = 0;
		this.status = DownloadStatus.READY;
	}

	public URL getUrl() {
		return url;
	}

	public File getTarget() {
		return target;
	}

	public StreamDownloadNotifier getNotifier() {
		return notifier;
	}

	public void setNotifier(StreamDownloadNotifier notifier) {
		this.notifier = notifier;
	}

	public int getLength() {
		return length;
	}

	public int getCount() {
		return count;
	}

	public DownloadStatus getStatus() {
		return status;
	}

	public void start() throws IOException {
		if (status == DownloadStatus.DOWNLOADING) throw new RuntimeException("This download is already running!");
		status = DownloadStatus.DOWNLOADING;
		RandomAccessFile randomAccessFile = null;
		try {

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestProperty("User-Agent", HTMLUtils.USER_AGENT);
			connection.setDoInput(true);

			target.createNewFile();
			randomAccessFile = new RandomAccessFile(target, "rw");

			byte[] bytes = new byte[BUF_SIZE];

			BufferedInputStream bufferedInputStream = new BufferedInputStream(connection.getInputStream());
			check(connection);

			length = connection.getContentLength();
			count = 0;

			int read;
			if (notifier != null) notifier.onStart(this);
			while ((read = bufferedInputStream.read(bytes)) > 0) {
				randomAccessFile.write(bytes, 0, read);
				count += read;

				if (notifier != null) notifier.onDownload(this);


				if (Thread.interrupted())
					throw new DownloadException("Thread interrupted");
			}
			bufferedInputStream.close();
		} finally {
			if (randomAccessFile != null)
				randomAccessFile.close();
		}
		status = DownloadStatus.DONE;
		if (notifier != null) notifier.onFinish(this);
	}


	static void check(HttpURLConnection c) throws IOException {
		int code = c.getResponseCode();
		switch (code) {
			case HttpURLConnection.HTTP_OK:
			case HttpURLConnection.HTTP_PARTIAL:
				return;
			case HttpURLConnection.HTTP_MOVED_TEMP:
			case HttpURLConnection.HTTP_MOVED_PERM:
				// rfc2616: the user agent MUST NOT automatically redirect the
				// request unless it can be confirmed by the user
				throw new RuntimeException("Download moved");
			case HttpURLConnection.HTTP_PROXY_AUTH:
				throw new RuntimeException("Proxy auth");
			case HttpURLConnection.HTTP_FORBIDDEN:
				throw new RuntimeException("Http forbidden: " + code);
			case 416:
				throw new RuntimeException("Requested range nt satisfiable");
		}
	}
}
