package com.degoos.soundclouddownloader.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTMLUtils {

	public static String USER_AGENT = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)";

	public static String getHtml(URL url) throws IOException {

		URLConnection connection = url.openConnection();
		connection.setRequestProperty("User-Agent", USER_AGENT);
		connection.setDoInput(true);

		InputStream is = connection.getInputStream();
		String enc = connection.getContentEncoding();
		if (enc == null) {
			Pattern p = Pattern.compile("charset=(.*)");
			Matcher m = p.matcher(connection.getHeaderField("Content-Type"));
			if (m.find()) enc = m.group(1);
			else enc = "UTF-8";
		}

		BufferedReader reader = new BufferedReader(new InputStreamReader(is, enc));

		String line;
		StringBuilder html = new StringBuilder();
		while ((line = reader.readLine()) != null) {
			html.append(line).append("\n");
			if (Thread.currentThread().isInterrupted())
				throw new RuntimeException("HTML download has been interrupted.");
		}

		return html.toString();
	}

}
