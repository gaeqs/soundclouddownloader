package com.degoos.soundclouddownloader.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IdUtils {

	public static String getId(String html) {
		Pattern pattern = Pattern.compile("soundcloud://sounds:(.*?)\"");
		Matcher matcher = pattern.matcher(html);
		if (matcher.find())
			return matcher.group(1);
		throw new NullPointerException("Couldn't find id");
	}

}
