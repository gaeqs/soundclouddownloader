package com.degoos.soundclouddownloader.object;

import com.degoos.soundclouddownloader.SoundCloudDownloader;
import com.degoos.soundclouddownloader.download.StreamDownload;
import com.degoos.soundclouddownloader.download.StreamDownloadNotifier;
import com.degoos.soundclouddownloader.util.HTMLUtils;
import com.degoos.soundclouddownloader.util.IdUtils;
import org.jooq.tools.json.JSONValue;
import org.jooq.tools.json.ParseException;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Map;

public class SoundCloudSong {

	private URL url;
	private String html, id;
	private URL stream;

	public SoundCloudSong(String url) throws IOException {
		this.url = new URL(url);
		this.html = HTMLUtils.getHtml(this.url);
		this.id = IdUtils.getId(html);
		this.stream = null;
	}


	public SoundCloudSong(URL url) throws IOException {
		this.url = url;
		this.html = HTMLUtils.getHtml(this.url);
		this.id = IdUtils.getId(html);
		this.stream = null;
	}

	public URL getUrl() {
		return url;
	}

	public String getHtml() {
		return html;
	}

	public String getId() {
		return id;
	}

	public URL getStream() {
		return stream;
	}

	public boolean isInfoExtracted() {
		return stream != null;
	}

	public void extractInfo() throws IOException, ParseException {
		URL url = new URL(SoundCloudDownloader.API.replace("{0}", id).replace("{1}", SoundCloudDownloader.CLIENT_ID));
		String json = HTMLUtils.getHtml(url);
		Map map = (Map) JSONValue.parseWithException(json);
		stream = new URL((String) map.get("http_mp3_128_url"));
	}


	public StreamDownload toStreamDownload(File targetFile, StreamDownloadNotifier notifier) throws IOException, ParseException {
		if (!isInfoExtracted()) extractInfo();
		return new StreamDownload(stream, targetFile, notifier);
	}

	public StreamDownload startDownload(File targetFile, StreamDownloadNotifier notifier) throws IOException, ParseException {
		StreamDownload download = toStreamDownload(targetFile, notifier);
		download.start();
		return download;
	}

}
